/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.brasileirao.dao;

import br.com.brasileirao.conexao.ConexaoBD;
import br.com.brasileirao.modelo.jogosTempAdm;
import br.com.brasileirao.util.FacesUtil;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Hilton
 */
public class DAO_JogosTempAdm implements IDAO_JogosTemp<jogosTempAdm>{

    @Override
    public void Cadastrar(jogosTempAdm t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void Apagar(jogosTempAdm t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void Atualizar(jogosTempAdm t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
  
    
    public List<jogosTempAdm> listarA() {
        
        String sql = "SELECT *FROM jogostemporadaadm WHERE Serie = 'a' order by data ;";
        List<jogosTempAdm> lista = new ArrayList<jogosTempAdm>();

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            while (rs.next()) {
                String time1 = rs.getString("time1");
                String golstime1 = rs.getString("golstime1");
                String time2 = rs.getString("time2");
                String golstime2 = rs.getString("golstime2");
                Date data = rs.getDate("data");
                Time hora  = rs.getTime("hora");

                jogosTempAdm jo = new jogosTempAdm();
                
                jo.setTime1(time1);
                System.out.println(time1);
                jo.setGolstime1(golstime1);
                jo.setTime2(time2);
                jo.setGolstime2(golstime2);
                jo.setData(data);
                jo.setHora(hora);
                
                
                
                lista.add(jo);
            }
            pst.close();
            ConexaoBD.Desconectar();
            return lista;

        } catch (Exception e) {
            // System.out.print("Não foi possível fazer a conexão com o banco");
            e.printStackTrace();
            FacesUtil.MensagemErro("Não foi possível fazer a listagem dos jogos");
            return null;
        }
    }
    
     public List<jogosTempAdm> listarB() {
        
        String sql = "SELECT *FROM jogostemporadaadm WHERE Serie = 'b' order by data ;";
        List<jogosTempAdm> lista = new ArrayList<jogosTempAdm>();

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            while (rs.next()) {
                String time1 = rs.getString("time1");
                String golstime1 = rs.getString("golstime1");
                String time2 = rs.getString("time2");
                String golstime2 = rs.getString("golstime2");
                Date data = rs.getDate("data");
                Time hora  = rs.getTime("hora");

                jogosTempAdm jo = new jogosTempAdm();
                
                jo.setTime1(time1);
                System.out.println(time1);
                jo.setGolstime1(golstime1);
                jo.setTime2(time2);
                jo.setGolstime2(golstime2);
                jo.setData(data);
                jo.setHora(hora);
                
                
                
                lista.add(jo);
            }
            pst.close();
            ConexaoBD.Desconectar();
            return lista;

        } catch (Exception e) {
            // System.out.print("Não foi possível fazer a conexão com o banco");
            e.printStackTrace();
            FacesUtil.MensagemErro("Não foi possível fazer a listagem dos jogos");
            return null;
        }
    }
}
