/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.brasileirao.dao;

import br.com.brasileirao.conexao.ConexaoBD;
import br.com.brasileirao.modelo.Usuario;
import br.com.brasileirao.util.FacesUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Hilton
 */

public class DAO_Usuario implements IDAO_Usuario <Usuario>{
    ConexaoBD conect = new ConexaoBD(); 
    
    /**
     *
     * @param u
     * @throws SQLException
     */
    
    @Override
    public void Cadastrar(Usuario u) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void Apagar(Usuario u) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void Atualizar(Usuario u) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Usuario autenticar(Usuario u) {
        String sql = "select * from usuario where usuario= '" + u.getUsuario()
                + "' and senha = '" + u.getSenha() + "';";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            if (rs.next()) {
                u.setCod(rs.getInt("cod"));
                u.setTipo(rs.getString("tipo"));
                u.setNome(rs.getString("nome"));
                
            } else {
                u.setNome("");
                FacesUtil.MensagemErro("Usuário ou senha incorretas!");
            }

            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }

        return u;
    }
    
    
}
