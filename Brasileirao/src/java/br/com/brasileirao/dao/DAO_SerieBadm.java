/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.brasileirao.dao;

import br.com.brasileirao.conexao.ConexaoBD;
import br.com.brasileirao.modelo.Serie;
import br.com.brasileirao.util.FacesUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Hilton
 */
public class DAO_SerieBadm implements IDAO_Serie <Serie>{

    ConexaoBD conect = new ConexaoBD(); 
    /**
     *
     * @param s
     * @throws SQLException
     */
    
    @Override
    public void Cadastrar(Serie s) throws SQLException {
        String sql = "INSERT INTO seriebadm (nometime, Pontos, Jogos, Vitorias, Empates, Derrotas, GolsPro, GolsContra, saldoGols) VALUES (?,?,?,?,?,?,?,?,?);";
        
        try{
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;

            pst = conn.prepareStatement(sql);
            
            pst.setString(1, s.getNomeTime());
            pst.setInt(2, s.getPontos());
            pst.setInt(3, s.getJogos());
            pst.setInt(4, s.getVitorias());
            pst.setInt(5, s.getEmpates());
            pst.setInt(6, s.getDerrotas());
            pst.setInt(7, s.getGolsPro());
            pst.setInt(8, s.getGolsContra());
            pst.setInt(9, s.getSaldoGols());
            
            pst.execute();
            pst.close();
            ConexaoBD.Desconectar();
            
            
        }catch (SQLException a) {
            a.printStackTrace();

        }
    }

    @Override
    public void Apagar(Serie s) throws SQLException {
        String sql = "DELETE FROM seriebadm WHERE nometime= ?;";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;

            pst = conn.prepareStatement(sql);
            pst.setString(1, s.getNomeTime());

            pst.execute();
            FacesUtil.MensagemIformativa("Time apagado com sucesso!");
            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
            FacesUtil.MensagemErro("Não foi possível apagar o time.");
        }
    }

    @Override
    public void Atualizar(Serie s) {

    }
    
    public void Atualizar1(Serie s, String nome) throws SQLException {
        String sql = "UPDATE seriebadm SET nometime= ?, Pontos= ?, Jogos= ?, Vitorias= ?, Empates= ?, Derrotas= ?, GolsPro= ?, GolsContra= ?, saldoGols= ?  WHERE nometime = '" + nome + "';";
        
        try{
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;

            pst = conn.prepareStatement(sql);
            
            pst.setString(1, s.getNomeTime());
            pst.setInt(2, s.getPontos());
            pst.setInt(3, s.getJogos());
            pst.setInt(4, s.getVitorias());
            pst.setInt(5, s.getEmpates());
            pst.setInt(6, s.getDerrotas());
            pst.setInt(7, s.getGolsPro());
            pst.setInt(8, s.getGolsContra());
            pst.setInt(9, s.getSaldoGols());
            
            pst.execute();
            pst.close();
            ConexaoBD.Desconectar();
            
            
        }catch (SQLException a) {
            a.printStackTrace();

        }
    }
    
    public List<Serie> listar() {
        int cont = 0;
        String sql = "SELECT *FROM seriebadm order by Pontos desc;";
        List<Serie> lista = new ArrayList<Serie>();

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            while (rs.next()) {
                String nometime = rs.getString("nometime");
                int Pontos = rs.getInt("Pontos");
                int Jogos = rs.getInt("Jogos");
                int Vitorias = rs.getInt("Vitorias");
                int Empates = rs.getInt("Empates");
                int Derrotas = rs.getInt("Derrotas");
                int GolsPro = rs.getInt("GolsPro");
                int GolsContra = rs.getInt("GolsContra");
                int saldoGols = rs.getInt("saldoGols");

                Serie se = new Serie();
                
                se.setNomeTime(nometime);
                se.setPontos(Pontos);
                se.setJogos(Jogos);
                se.setVitorias(Vitorias);
                se.setEmpates(Empates);
                se.setDerrotas(Derrotas);
                se.setGolsPro(GolsPro);
                se.setGolsContra(GolsContra);
                se.setSaldoGols(saldoGols);
                
                cont++;
                se.setColocacao(cont);
                lista.add(se);
            }
            pst.close();
            ConexaoBD.Desconectar();
            return lista;

        } catch (Exception e) {
            // System.out.print("Não foi possível fazer a conexão com o banco");
            e.printStackTrace();
            FacesUtil.MensagemErro("Não foi possível fazer a listagem dos times");
            return null;
        }
    }
    
}
