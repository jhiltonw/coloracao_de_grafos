/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Hilton
 * Created: 22/10/2018
 */
create database brasileirao;

use brasileirao;

create table usuario(cod integer auto_increment primary key, nome varchar(50) not null, usuario varchar(30), senha varchar(30), tipo varchar(1));

create table serieAadm (nometime varchar(40)not null primary key unique, Pontos integer, Jogos integer,
Vitorias integer, Empates integer, Derrotas integer, GolsPro integer, GolsContra integer,
saldoGols integer);

create table serieBadm (nometime varchar(40)not null primary key unique, Pontos integer, Jogos integer,
Vitorias integer, Empates integer, Derrotas integer, GolsPro integer, GolsContra integer,
saldoGols integer);

create table serieAsimu (cod integer primary key, cod_usuario integer, nometime varchar(40)not null, Pontos integer, Jogos integer,
Vitorias integer, Empates integer, Derrotas integer, GolsPro integer, GolsContra integer,
saldoGols integer, foreign key(cod_usuario) references usuario(cod));

create table serieBsimu (cod integer primary key, cod_usuario integer, nometime varchar(40)not null, Pontos integer, Jogos integer,
Vitorias integer, Empates integer, Derrotas integer, GolsPro integer, GolsContra integer,
saldoGols integer, foreign key(cod_usuario) references usuario(cod));





create table jogostemporadaAdm(cod integer primary key auto_increment, time1 varchar(20) not null, golstime1 varchar(2), time2 varchar(20), golstime2 varchar(2), data date, hora time,Serie varchar(1));



create table jogostemporadasimu(cod integer primary key auto_increment, cod_usuario integer, time1 varchar(20) not null, golstime1 varchar(2), 
time2 varchar(20), golstime2 varchar(2),  data date, hora time, Serie varchar(1), foreign key(cod_usuario) references usuario(cod));
