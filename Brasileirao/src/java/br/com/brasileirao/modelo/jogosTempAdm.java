/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.brasileirao.modelo;


import java.sql.Date;
import java.sql.Time;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Hilton
 */
@ManagedBean(name = "jogosTemp")
@SessionScoped

public class jogosTempAdm {
    
    private Integer cod;
    private String time1;
    private String golstime1;
    private String time2;
    private String golstime2;
    private Date data;
    private Time hora;

    public Integer getCod() {
        return cod;
    }

    public void setCod(Integer cod) {
        this.cod = cod;
    }

    public String getTime1() {
        return time1;
    }

    public void setTime1(String time1) {
        this.time1 = time1;
    }

    public String getGolstime1() {
        return golstime1;
    }

    public void setGolstime1(String golstime1) {
        this.golstime1 = golstime1;
    }

    public String getTime2() {
        return time2;
    }

    public void setTime2(String time2) {
        this.time2 = time2;
    }

    public String getGolstime2() {
        return golstime2;
    }

    public void setGolstime2(String golstime2) {
        this.golstime2 = golstime2;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Time getHora() {
        return hora;
    }

    public void setHora(Time hora) {
        this.hora = hora;
    }
    
    
}
