/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.brasileirao.beans;

import br.com.brasileirao.util.FacesUtil;
import br.com.brasileirao.dao.DAO_Usuario;
import br.com.brasileirao.modelo.Usuario;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Hilton
 */

@ManagedBean(name = "validaUsuario")
@SessionScoped

public class ValidaUsuarioBean {
    private Usuario usuarioLogado = null;
    private boolean log = false;
    
    private String usuario;
    private String senha;

    public ValidaUsuarioBean() {
        
    }

    

    public void logar() {

        DAO_Usuario dao = new DAO_Usuario();
       
        Usuario logado = dao.autenticar(new Usuario(usuario,senha));

        if (!logado.getNome().equals("")) {
            
            
            log = true;
            
            if (logado.getTipo().equals("1")) {
                usuarioLogado = logado;
                
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("LogadoAdm.xhtml");
                } catch (IOException ex) {
                    Logger.getLogger(ValidaUsuarioBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            if (logado.getTipo().equals("2"))  {
                usuarioLogado = logado;
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("LogadoUser.xhtml");
                } catch (IOException ex) {
                    Logger.getLogger(ValidaUsuarioBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }     
             
        }else{
            log = false;
            FacesUtil.MensagemIformativa("Usuário ou senha incorretos!");
             
        }
    }
    
    /*public void testeLog(){
        if(this.log == false){
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("Login.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(ValidaUsuarioBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }   
    }*/
    
    public void testeLogUser(){
        if(this.log == false){
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("Login.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(ValidaUsuarioBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            if(this.usuarioLogado.getTipo().equals("2")){
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("Login.xhtml");
                } catch (IOException ex) {
                    Logger.getLogger(ValidaUsuarioBean.class.getName()).log(Level.SEVERE, null, ex);
                 }
            }
        }
    }
    
    public void testeLogAdm(){
        if(this.log == false){
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("Login.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(ValidaUsuarioBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            if(this.usuarioLogado.getTipo().equals("1")){
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("Login.xhtml");
                } catch (IOException ex) {
                    Logger.getLogger(ValidaUsuarioBean.class.getName()).log(Level.SEVERE, null, ex);
                 }
            }
        }
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    public Usuario getUsuarioLogado() {
        return usuarioLogado;
    }

    public void setUsuarioLogado(Usuario u) {
        this.usuarioLogado = u;
    }
    
     public boolean isLog() {
        return log;
    }

    public void setLog(boolean log) {
        this.log = log;
    }
    
}
