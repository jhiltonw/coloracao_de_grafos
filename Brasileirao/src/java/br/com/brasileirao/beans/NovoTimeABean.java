/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.brasileirao.beans;

import br.com.brasileirao.util.FacesUtil;
import br.com.brasileirao.dao.DAO_SerieAadm;
import br.com.brasileirao.modelo.Serie;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.swing.JOptionPane;

/**
 *
 * @author Hilton
 */
@ManagedBean(name = "CrudTimeA")
@SessionScoped

public class NovoTimeABean {

    private Serie novotime;

    public void salvarTime(Serie s){

        DAO_SerieAadm dao = new DAO_SerieAadm();

        try {
            dao.Cadastrar(s);
            novotime = s;
            FacesUtil.MensagemIformativa("Time salvo com sucesso!");
            //limpa os dados da reserva para não aparecer no formulário
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudTimeA");
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("s");
            FacesContext.getCurrentInstance().getExternalContext().redirect("AtualizarAAdm.xhtml");
           

        } catch (SQLException ex) {
            System.out.println("Erro de SQL: " + ex);
            FacesUtil.MensagemErro("Não foi possível adicionar um novo time! :/");

        } catch (IOException ex) {
            Logger.getLogger(NovoTimeABean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void excluirTime(Serie s) {
        DAO_SerieAadm dao = new DAO_SerieAadm();

        try {
            dao.Apagar(s);

            FacesUtil.MensagemIformativa("Time apagado com sucesso!");
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudTimeA");
            FacesContext.getCurrentInstance().getExternalContext().redirect("AtualizarAAdm.xhtml");
        } catch (SQLException ex) {
            System.out.println("Erro de SQL: " + ex);
            FacesUtil.MensagemErro("Não foi possível excluir o time! :/");
        } catch (IOException ex) {
            Logger.getLogger(NovoTimeABean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
