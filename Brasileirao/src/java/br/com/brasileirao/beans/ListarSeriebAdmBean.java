/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.brasileirao.beans;

import br.com.brasileirao.dao.DAO_SerieBadm;
import br.com.brasileirao.modelo.Serie;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Hilton
 */
@ManagedBean(name = "listaSerieBadm")
@SessionScoped


public class ListarSeriebAdmBean {
    
    private List<Serie> lista = new ArrayList<Serie>();
    
    public ListarSeriebAdmBean(){}
    
    public List<Serie> getLista() {
        DAO_SerieBadm dao = new DAO_SerieBadm();
        this.lista = dao.listar();
       
        return lista;
    }
    public void setLista(List<Serie> lista) {
        this.lista = lista;
    }
}
