/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.brasileirao.beans;

import br.com.brasileirao.dao.DAO_JogosTempAdm;
import br.com.brasileirao.modelo.jogosTempAdm;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Hilton
 */
@ManagedBean(name = "listaJogosTempAdm")
@SessionScoped
public class ListarJogosTempAadmBean {
    
       private List<jogosTempAdm> lista = new ArrayList<jogosTempAdm>();
    
    public ListarJogosTempAadmBean(){}
    
    public List<jogosTempAdm> getListaA() {
        DAO_JogosTempAdm dao = new DAO_JogosTempAdm();
        this.lista = dao.listarA();
        
        return lista;
    }
    
    
    public List<jogosTempAdm> getListaB() {
        DAO_JogosTempAdm dao = new DAO_JogosTempAdm();
        this.lista = dao.listarB();
        
        return lista;
    }
    
    
        public String ConvData(Date d){  
       SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); // formato de data desejado
       String s = sdf.format(d);
       
       return s;
    }
}
