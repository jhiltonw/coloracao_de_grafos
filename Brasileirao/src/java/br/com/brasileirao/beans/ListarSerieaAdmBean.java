/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.brasileirao.beans;

import br.com.brasileirao.dao.DAO_SerieAadm;
import br.com.brasileirao.modelo.Serie;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Hilton
 */
@ManagedBean(name = "listaSerieAadm")
@SessionScoped

public class ListarSerieaAdmBean {
    
    private List<Serie> lista = new ArrayList<Serie>();
    private List<String> listaNome = new ArrayList<String>();
    public ListarSerieaAdmBean(){}
    
    public List<Serie> getLista() {
        DAO_SerieAadm dao = new DAO_SerieAadm();
        this.lista = dao.listar();
       
        return lista;
    }
    
    public List<String> getListanome() {
        DAO_SerieAadm dao = new DAO_SerieAadm();
        this.listaNome = dao.listarNome();
       
        return listaNome;
    }
   
}
