/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coloracao;

import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author Hilton
 */
public class Colore {

    /**
     * @param args the command line arguments
     */
    ArrayList<Vertice> vertices;
    ArrayList<Aresta> arestas;
    ArrayList<Cor> cores;
    Scanner teclado = new Scanner(System.in);
    
    public static void main(String[] args) {
        // TODO code application logic here
        Colore c = new Colore();
        c.Inicia();
        c.imprimrGrafo();
    }
    
    private void Inicia(){
        vertices = new ArrayList<Vertice>();
        arestas = new ArrayList<Aresta>();
        cores = new ArrayList<Cor>();
        //pedi o numero de vertices e adiciona ao array de vertices
        
        System.out.print("Digite a quantidade de vertices: ");
        int qvertices = teclado.nextInt();
        //coverte os vertices para letras
        int q;
        for(int i = 0; i < qvertices; i++){
            q = 65 + i;
            char letra = (char) q;
            vertices.add(new Vertice(letra));
        }
        
        q = 65 + vertices.size()-1;
        char ultimo = (char)q;
        
        //string que irá receber a entrada
        String arestaEntrada = "";
        //variavel temporaria para modificar a entrad sem afestar a entrada original
        String arestaTemp;
        
        boolean repeti = true;
        
        do{
            repeti = true;
            //recebe as arestas
            arestaEntrada = JOptionPane.showInputDialog(null,"Defina as aresta de A..."+ultimo+" separando por virgula. Exemplo: AB,AC,BC: ");
            arestaEntrada = arestaEntrada.toUpperCase();
            arestaEntrada = arestaEntrada.replace(" ", "");
            arestaEntrada = arestaEntrada.replace(";", ",");
            arestaEntrada = arestaEntrada.replace(".", ",");
            
            if(arestaEntrada.substring(0, 1).equals(",")){
                arestaEntrada = arestaEntrada.substring(1);
                
            }if(arestaEntrada.substring(arestaEntrada.length()-1).equals(",")){
                arestaEntrada = arestaEntrada.substring(0, arestaEntrada.length()-1);
                
            }
            arestaTemp = arestaEntrada.replace(",","");
            
            char cv[] = arestaTemp.toCharArray();
            
            for(char d : cv){
                if(d > ultimo){
                    System.out.println("O vertice: "+d+" é inválido.");
                    repeti = false;
                    
                }
            }
            arestaTemp = arestaEntrada;
            
            do{
                String av = "";
                if(arestaTemp.indexOf(",")>- 1){
                    av = arestaTemp.substring(0, arestaTemp.indexOf(","));
                    arestaTemp = arestaTemp.substring(arestaTemp.indexOf(",")+1);
                }else{
                    av = arestaTemp;
                    arestaTemp = "";
                }if(av.length()> 2){
                    System.out.println("Arestas são definidas de um");
                    repeti = false;
                }
            }while(arestaTemp.length() > 0);    
            
            //se na entrada for dada alguma aresta inválida, o programa notifica e pedi novamente a entrada.
        }while(!repeti);
        
        arestaTemp = arestaEntrada;
        //popula lista de arestas.
        do{
            String av = "";
            if(arestaTemp.indexOf(",") > 1){
                av = arestaTemp.substring(0, arestaTemp.indexOf(","));
                arestaTemp = arestaTemp.substring(arestaTemp.indexOf(",") + 1);
            }else{
                av = arestaTemp;
                arestaTemp = "";
            }
        arestas.add(new Aresta(buscarVertice(av.substring(0,1)),buscarVertice(av.substring(1,2))));
        }while(arestaTemp.length() > 0);
        
        //colocar cores na lista
        adicionaCores();
        
        //colorir os vertices
        for(Vertice vi : vertices){
            Colorir(vi);
        }
        
        
         
    }
    
    private Vertice buscarVertice(String n){
            Vertice retorno = null;
            for(Vertice v : vertices){
                if(v.getNome().equals(n)){
                    retorno = v;
                    break;
                }
            }
            return retorno;
        }
    //metodo preenche arestaEntrada lista de cores
    private void adicionaCores(){
        cores.clear();
        cores.add(new Cor("Azul"));
        cores.add(new Cor("Vermelho"));
        cores.add(new Cor("Amarelo"));
        cores.add(new Cor("Verde"));
    }
    
    //metodo que colore o vertice verificando o vertice de origem para não repetir a cor
    private void Colorir(Vertice vk){
        Cor color;
        color = null;
        if(!vk.colorido()){
            for(Cor cc : cores){
                boolean corLivre = true;
                for(Vertice vj : vk.getAdjacencia()){
                    if(vj.colorido()){
                        if(vj.getCor().equals(cc)){
                            corLivre = false;
                            break;
                        }
                    }
                }
                if(corLivre){
                    color = cc;
                    break;
                }
            }
            vk.setCor(color);
            for(Vertice vj : vk.getAdjacencia()){
                Colorir(vj);
            }
        }
    }
    //imprime a cor de cada vertice e depos a ligação do vertice de origem para o de destino no formato A --> B
    private void imprimrGrafo(){
        String texto = "";
        String texto2 = "";
        for(Vertice v : vertices){
            texto+= ""+ v.getNome()+" Cor: "+v.getCor().getCor()+"\n";
        }
        System.out.println(texto);
        for(Aresta a : arestas){
            texto2+= a.getvOrigem().getNome()+" --> "+a.getvDestino().getNome()+"\n";
        }
        System.out.println(texto2);
    }
}