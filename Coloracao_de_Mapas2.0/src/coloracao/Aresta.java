/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coloracao;

/**
 *
 * @author Hilton
 */
public class Aresta {
    private String nome;
    private Vertice vOrigem;
    private Vertice vDestino;
    
    public Aresta(String n, Vertice o, Vertice d){
        nome = "";
        vOrigem = o;
        vDestino = d;
    }
    
    public Aresta(Vertice o, Vertice d){
        vOrigem = o;
        vOrigem.adicionarAdjacente(d);
        vDestino = d;
        vDestino.adicionarAdjacente(o);
    }

    public String getNome() {
        return nome;
    }

    public Vertice getvOrigem() {
        return vOrigem;
    }

    public Vertice getvDestino() {
        return vDestino;
    }
    
    
}