/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coloracao;

import java.util.ArrayList;

/**
 *
 * @author Hilton
 */
public class Vertice {
    private String nome;
    private ArrayList<Vertice> adjacencia;
    private Cor cor;
    
    public Vertice(){
        nome = "";
        adjacencia = new ArrayList<Vertice>();
        cor = null;
    }
    public Vertice(char n){
        nome = ""+n;
        adjacencia = new ArrayList<Vertice>();
        cor = null;
    }

    public String getNome() {
        return nome;
    }

    public void setCor(Cor cor) {
        this.cor = cor;
    }
    
    
    
    public Cor getCor() {
        return cor;
    }
    
    

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public void adicionarAdjacente(Vertice v){
        adjacencia.add(v);
    }

    public ArrayList<Vertice> getAdjacencia() {
        return adjacencia;
    }
    
    
    
    public int getGrau(){
        return(adjacencia.size());
    }
    
    public boolean colorido(){
        if(cor != null){
            return true;
        }else{
            return false;
        }
    }
    
}
